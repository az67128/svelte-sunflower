### Sunflower charity widget

Widget for [fondpodsolnuh.ru](https://www.fondpodsolnuh.ru/)

[Try demo](https://az67128.gitlab.io/svelte-sunflower/)

Install on your site:

```
<div id="sunflowerCharity"></div>
<script src="https://az67128.gitlab.io/svelte-sunflower/build/bundle.js"></script>
```

![preview](public/assets/preview.png)
