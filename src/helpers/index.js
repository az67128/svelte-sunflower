import audioFiles from "../assets/audioFiles";

// export const isSunflower = Math.round(Math.random());
const audioBlobs = {};
(() => {
  Object.keys(audioFiles).forEach(type => {
    audioFiles[type].forEach((item, i) => {
      fetch(`https://az67128.gitlab.io/svelte-sunflower/assets/${item}.mp3`)
        .then(res => res.blob())
        .then(blob => window.URL.createObjectURL(blob))
        .then(blob => {
          if (!audioBlobs[type]) audioBlobs[type] = [];
          audioBlobs[type].push(blob);
        });
    });
  });
})();

export const isSunflower = false;
const ym =
  window.ym || ((id, type, name, payload) => console.log(name, payload));

export const sendMetrik = (name, payload) =>
  ym(53183122, "reachGoal", `${name}${isSunflower ? "" : "_button"}`, payload);

export const payByCard = function(amount, onSuccess = () => {}) {
  sendMetrik("PaymentCard");
  setTimeout(() => {
    var widget = new cp.CloudPayments();
    widget.charge(
      {
        publicId: "pk_e6cd088c3cc349d5b9125035ee354",
        description: "Благотворительное пожертвование в фонд Подсолнух",
        amount: amount,
        currency: "RUB",
        skin: "mini"
      },
      function() {
        sendMetrik("PaymentCardSuccess");
        onSuccess();
      },
      function(reason, options) {
        sendMetrik("PaymentCardFailed");
        console.log("payment fail", reason);
      }
    );
  }, 100);
};

export const playSound = async type => {
  const audioElement = document.getElementById("sunflowerAudio");
  try {
    audioElement.muted = true;
    audioElement.play();
    audioElement.pause();
    audioElement.muted = false;
    audioElement.currentTime = 0;
  } catch (err) {
    // nothing to do
  }

  const auidoItem = audioBlobs[type]
    ? audioBlobs[type][Math.floor(Math.random() * audioBlobs[type].length)]
    : null;
  if (!auidoItem) return;
  audioElement.src = auidoItem;

  audioElement.play();
};

const isInViewport = function(el) {
  var top = el.offsetTop;
  var left = el.offsetLeft;
  var width = el.offsetWidth;
  var height = el.offsetHeight;

  while (el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }

  return (
    top < window.pageYOffset + window.innerHeight &&
    left < window.pageXOffset + window.innerWidth &&
    top + height > window.pageYOffset &&
    left + width > window.pageXOffset
  );
};

export const sayHelloIfVisible = (node => {
  let isPlayed = false;
  return node => {
    if (isPlayed) return;
    if (isInViewport(node)) {
      sendMetrik("ButtonOnstart");
      isPlayed = true;
      playSound("hello");
    }
  };
})();

export const payBySms = amount => {
  sendMetrik("PaymentSMS");
  const isIos =
    /iPad|iPhone|iPod|Mac/.test(navigator.userAgent) && !window.MSStream;
  const href = `sms:3443${isIos ? "&" : "?"}body=подсолнух ${Math.floor(
    amount
  )}`;
  window.open(href, "_self");
};
