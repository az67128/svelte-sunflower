import { writable, readable, derived, get } from "svelte/store";
import { sendMetrik, playSound, payBySms, payByCard } from "../helpers";

export const amount = writable(0);
export const intervalId = writable(null);
export const minimalAmount = 20; //75

export const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
  navigator.userAgent
);

export const showPressMore = derived(
  [intervalId, amount],
  ([$intervalId, $amount]) =>
    !$intervalId && $amount < minimalAmount && $amount > 0
);

export const addTime = () => {
  const currentAmount = get(amount);
  if (currentAmount && currentAmount % 150 === 0) playSound("hold");
  amount.update(state => (state += 0.25));
};

export const buttonStart = () => {
  sendMetrik("ButtonDown");
  playSound("down");

  clearInterval(get(intervalId));

  intervalId.set(setInterval(addTime, 10));
};

export const buttonEnd = () => {
  if(!get(intervalId)) return;
  clearInterval(get(intervalId));
  const currentAmount = get(amount);
  intervalId.set(null);
  if (currentAmount >= minimalAmount) {
    sendMetrik("ButtonUp", { value: Math.floor(currentAmount) });
    playSound("success");
    if (isMobile) {
      payBySms(Math.floor(currentAmount));
    } else {
      payByCard(Math.floor(currentAmount));
    }
  } else {
    playSound("error");
    sendMetrik("ButtonFailed");
  }
};
